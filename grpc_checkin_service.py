from __future__ import print_function
import traceback
import numpy as np
import config
import utils
import time
from grpc_classes import face_recog_pb2, face_recog_pb2_grpc
from concurrent import futures
import grpc
from grd_face_recognition.core import face_directions as fd
from grd_face_recognition.core.utils import preprocess_image
from grd_face_recognition.core.face_recognition import FaceRecognition
import sys
import multiprocessing
import base64
import os.path

multiprocessing.freeze_support()

sys.path.append("face_recognition_core")


MAX_MESSAGE_LENGTH = 500 * 1024 * 1024  # 500MB
face_recognition = FaceRecognition(data_path=config.DATA_PATH)


class FaceRecogService(face_recog_pb2_grpc.FaceRecogServiceServicer):
    def upload(self, request, context):
        try:
            imgdata = base64.b64decode(request.image)
            dir_name = request.name
            ts = time.time()
            pathName = os.path.join('data/pending_training', dir_name)
            if not os.path.isdir(pathName):
                os.makedirs(pathName)
            with open(os.path.join(pathName, f'{str(ts)}.jpg'), 'wb') as f:
                f.write(imgdata)
            response = face_recog_pb2.UploadResponse(
                is_satisfied=True, mess='Succ', is_error=False)
            return response
        except:
            response = face_recog_pb2.UploadResponse(
                is_satisfied=False, mess='Fail', is_error=True)
            return response

    def recognize(self, request, context):
        '''request.frames: list of RGB images '''
        response = face_recog_pb2.RecognizeResult()
        try:
            frame = utils.decode_b64_to_np_array(request.frames)[0]
            frame = frame[:, :, ::-1]
            frame = preprocess_image(
                frame, image_size=config.MAX_IMAGE_SIZE, padding=True)
            names, paths = face_recognition.recognize(frame)
            is_recognized = True if len(names) > 0 else False

            response.is_recognized = is_recognized
            response.names.extend(names)
            response.urls.extend(paths)
        except:
            print(traceback.print_exc())
            response.is_recognized = False
        return response

    def check_diversity(self, request, context):
        response = face_recog_pb2.DiversityResponse()
        if not context.is_active():
            response.is_satisfied = False
            response.mess = "Thao tác không thành công"
            return response
        try:
            path_to_images = request.path
            directions = np.array(config.directions)
            missing_dirs = np.array(fd.get_require_direction(path_to_images))
            if len(missing_dirs) != 0:
                missing_dirs = directions[missing_dirs]
                is_satisfied = False
                response.mess = "Ảnh thiếu các hướng " + \
                    ', '.join(missing_dirs.tolist())
            else:
                is_satisfied = True
                response.mess = "Thoả mãn"
            response.is_satisfied = is_satisfied
        except:
            print(traceback.print_exc())
            response.is_satisfied = False
            response.mess = "Đã xảy ra lỗi trong quá trình xử lý"
        return response

    def train(self, request, context):
        mess = ''
        response = face_recog_pb2.TrainingResult()
        try:
            is_successful = face_recognition.train()
            if is_successful:
                mess = 'Huấn luyện thành công'
        except:
            print(traceback.print_exc())
            is_successful = False
            mess = 'Đã xảy ra lỗi trong quá trình xử lý'
        response.is_success = is_successful
        response.mess = mess
        return response

    def delete(self, request, context):
        try:
            is_success = face_recognition.delete(request.name)
        except:
            print(traceback.print_exc())
            is_success = False
        if not is_success:
            mess = 'Lỗi trong quá trình xóa dữ liệu'
        else:
            mess = ''
        response = face_recog_pb2.DeleteResponse(
            is_success=is_success, mess=mess)
        return response


server = grpc.server(futures.ThreadPoolExecutor(max_workers=20),  options=[
    ('grpc.max_send_message_length', MAX_MESSAGE_LENGTH),
    ('grpc.max_receive_message_length', MAX_MESSAGE_LENGTH)
])
face_recog_pb2_grpc.add_FaceRecogServiceServicer_to_server(
    FaceRecogService(), server)

print("Starting server. Listening on port " + str(config.GRPC_PORT))
server.add_insecure_port('[::]:' + str(config.GRPC_PORT))
server.start()

try:
    while True:
        time.sleep(86400)
except KeyboardInterrupt:
    server.stop(0)
