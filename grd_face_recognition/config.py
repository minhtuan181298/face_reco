# -*- coding: utf-8 -*-

thrift_emb_service_host = '0.0.0.0'
thrift_emb_service_port = 9991

N_COMPONENTS_PCA = 128

MIN_NUM_PEOPLE = 3
MAX_NUM_TRAINING_IMAGES = 80
MIN_NUM_TRAINING_IMAGES = 8
MAX_IMAGE_SIZE = 800

MTCNN_THRESHOLD = [0.3, 0.6, 0.9]
MTCNN_FACTOR = 0.6

DISPLAY_WIDTH = 1500
FACE_MIN_SIZE = (80, 80)
FACE_MAX_SIZE = (800, 800)

mark_processed = 'processed'
