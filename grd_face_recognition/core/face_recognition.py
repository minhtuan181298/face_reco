import os
import cv2
import sys
import shutil
import datetime
import traceback

from grd_face_recognition import config
from grd_face_recognition.core.predict_module import PredictModule
from grd_face_recognition.core.train_predict_model import start_training
from grd_face_recognition.core.face_directions import get_require_direction
from grd_face_recognition.core.utils import is_good, preprocess_image, gen_random_str
from grd_face_recognition.core import constants
from grd_face_recognition.face_detection.face_detection_mtcnn import detect_face_all_directions
from grd_face_recognition.feature_extraction.resnet import get_embedding


def generate_response(predict_result):
    names = []
    # is_recognized = False
    for i in range(len(predict_result)):
        if predict_result[i][0] is None:  # unrecognized
            names.append("unrecognized")
        elif predict_result[i][0] == '':  # newcomer
            names.append("newcomer")
            # is_recognized = True
        else:  # have id
            # names.append(str(predict_result[i][0]) + ' ' + str(predict_result[i][1]))
            names.append(str(predict_result[i][0]))
            # is_recognized = True
    return names


class FaceRecognition:
    def __init__(self, data_path):
        self.MIN_SIZE = config.FACE_MIN_SIZE
        self.MAX_SIZE = config.FACE_MAX_SIZE
        self.svm_model = None
        self.constants = type('', (), {})()
        self.data_path = data_path

        self.get_constants()
        self.create_dirs()

    def create_dirs(self):
        for prop in dir(self.constants):
            if prop.split('_')[-1] == 'dir' and \
                    not os.path.exists(self.constants.__dict__[prop]):
                os.makedirs(self.constants.__dict__[prop])

    def get_constants(self):
        for prop in dir(constants):
            if str(prop)[:2] == '__':
                continue
            self.constants.__dict__[prop] = os.path.join(
                self.data_path, constants.__dict__[prop])

    def train(self):
        if len(os.listdir(self.constants.face_pending_dir)) + \
                len(os.listdir(self.constants.face_cluster_dir)) < config.MIN_NUM_PEOPLE:
            print("Not enough people in data for training")
            return False
        try:
            start_training(self.constants)
            self.svm_model = PredictModule(self.constants.model_path)
            return True
        except:
            print(traceback.print_exc())
            return False

    def recognize(self, image):
        ''' image: BGR Image '''
        location, _, angle = detect_face_all_directions(image.copy())
        image = preprocess_image(image, rotation_angle=angle)
        all_faces = []
        all_location = []
        for i in range(len(location)):
            (l, t, r, b) = location[i]
            face = image[t:b, l:r].copy()
            if face.shape[0] > 0 and face.shape[1] > 0:
                all_faces.append(face)
                all_location.append((l, t, r, b))
        face_send = []
        for i in range(len(all_faces)):
            face_send.append(all_faces[i])

        if len(face_send) > 0:
            tmp_vecs = get_embedding(face_send)
        else:
            tmp_vecs = []
        file_paths, predict_result = [], []
        if self.svm_model is None:
            self.svm_model = PredictModule(self.constants.model_path)
        for idx, vec in enumerate(tmp_vecs):
            name = self.svm_model.predict([vec])[0]
            predict_result.append([name])
            file_paths.append(self.save_face_for_review(name, image))
        if len(file_paths) == 0:
            file_paths.append(self.save_face_for_review(None, image))
        names = generate_response(predict_result)
        return names, file_paths

    def add(self, images, name):
        '''
            images: list of BGR images
            name: name of person who need to be added
        '''
        is_error, is_satisfied = False, False
        if name in os.listdir(self.constants.face_pending_dir):
            is_error = True
            print("Data already exists")
            return is_error, is_satisfied, None
        else:
            face_detection_threshold = [0.1, 0.5, 0.9]
            save_path = os.path.join(self.constants.face_upload_dir, name)
            if not os.path.exists(save_path):
                os.makedirs(save_path)
            for image in images:
                cv2.imwrite(os.path.join(
                    save_path, gen_random_str() + '.jpg'), image)
            missing_dirs = get_require_direction(save_path, self.constants.face_upload_raw_dir,
                                                 face_detection_threshold)
            if len(missing_dirs) != 0:
                is_satisfied = False
            else:
                is_satisfied = True
                self.copy_to_pending(save_path)
        return is_error, is_satisfied, missing_dirs

    def delete(self, name):
        face_dir = os.path.join(self.constants.face_cluster_dir, name)
        vector_dir = os.path.join(self.constants.vector_dir, name)
        sample_dir = os.path.join(self.constants.face_sample_dir, name)
        try:
            shutil.rmtree(face_dir)
            shutil.rmtree(vector_dir)
            shutil.rmtree(sample_dir)
            print("Delete successfully: " + name)
            return True
        except:
            print(traceback.print_exc())
            return False

    def save_face_for_review(self, name, image):
        if name is None:
            name = 'unrecognized'
        elif name == '':
            name = 'newcomer'
        now = datetime.datetime.now()
        date = str(now).split()[0]
        file_path = os.path.join(self.constants.face_review_dir, date, name)
        if not os.path.exists(file_path):
            os.makedirs(file_path)
        file_path = os.path.join(file_path, str(
            now).replace('.', '_') + '.jpg')
        cv2.imwrite(file_path, image)
        return file_path

    def copy_to_pending(self, path_to_images):
        username = path_to_images.split(os.sep)[-1]
        target = os.path.join(self.constants.face_pending_dir, username)
        if not os.path.exists(target):
            os.makedirs(target)
        for image_file in os.listdir(path_to_images):
            if image_file.split('_')[-1].split('.')[0] == config.mark_processed:
                shutil.move(os.path.join(path_to_images, image_file), target)
            else:
                os.remove(os.path.join(path_to_images, image_file))
        print("Copied images from upload folder to pending training")
