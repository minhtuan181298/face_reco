try:
    from setuptools import setup
    from setuptools import Extension
except ImportError:
    from distutils.core import setup
    from distutils.extension import Extension

from Cython.Distutils import build_ext
import numpy as np
import os


abs_path_model = os.path.abspath(os.path.dirname(__file__))
filepath = os.path.join(abs_path_model, 'predict_model.c')
ext_modules = [Extension("predict_model",[filepath])]

def run():
    setup(  
        name= 'Generic model class',
        cmdclass = {'build_ext': build_ext},
        include_dirs = [np.get_include()],
        ext_modules = ext_modules)

run()
