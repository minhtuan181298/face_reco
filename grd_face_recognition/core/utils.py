# -*- coding: utf-8 -*-
"""Utility functions."""
from __future__ import print_function
import math
import cv2
import numpy as np
import imutils
from numpy import array, dot, arccos, clip
from numpy.linalg import norm
import uuid
import datetime


def low_quality(img, threshold=[60, 300, 60]):
	"""Check if image is blur."""
	threshold_1, threshold_2, threshold_3 = threshold
	size = 40
	img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	img = cv2.GaussianBlur(img, (3, 3), 0)
	img = cv2.resize(img, (size, size))
	lap = cv2.Laplacian(img, cv2.CV_64F)
	lap_list = []
	for r in range(size):
		for c in range(size):
			if math.sqrt((size/2-r)**2+(size/2-c)**2) < size/2:
				lap_list.append(lap[r, c])

	lap_list = sorted(lap_list)
	lap_max = sum(lap_list[-30:]) / 30
	if lap_max < threshold_1:  # temporary value
		# if lap_max < 70:  # choosen value
		return True

	lap_list = np.array(lap_list)
	lap_var = lap.var()
	if lap_var < threshold_2:  # temporary value
		# if lap_var < 350:  # choosen value
		return True

	mean = np.mean(img)
	if mean < threshold_3:  # temporary value
		# if mean < 70:  # choosen value
		return True

	return False


def is_good(face, points):
	if low_quality(face):
		return False

	x1 = points[0]
	y1 = points[0 + 5]
	x4 = points[1]
	y4 = points[1 + 5]
	x_nose = points[2]
	y_nose = points[2 + 5]
	x3 = points[3]
	y3 = points[3 + 5]
	x2 = points[4]
	y2 = points[4 + 5]

	xi = ((x1*y2-y1*x2)*(x3-x4) - (x1-x2)*(x3*y4-y3*x4))/((x1-x2)*(y3-y4)-(y1-y2)*(x3-x4))
	yi = ((x1*y2-y1*x2)*(y3-y4) - (y1-y2)*(x3*y4-y3*x4))/((x1-x2)*(y3-y4)-(y1-y2)*(x3-x4))

	length = math.sqrt((xi-x_nose)**2+(yi-y_nose)**2)

	d1 = math.sqrt((x1-x3)**2+(y1-y3)**2)
	d2 = math.sqrt((x3-x2)**2+(y3-y2)**2)
	d3 = math.sqrt((x2-x4)**2+(y2-y4)**2)
	d4 = math.sqrt((x4-x1)**2+(y4-y1)**2)
	norm = (d1 + d2 + d3 + d4) / 4
	# norm = max(d1, d2, d3, d4)

	score = length / norm

	# if score > 0.2:  # choosen value
	if score > 0.25:  # temporary value
		return False

	dx = abs(x1 - x4)
	dy = abs(y1 - y4)

	if dy / dx > (1.0 / 3):  # temporary value
		return False

	return True


def preprocess_image(image, image_size=None, rotation_angle=None, padding=False):
	if image_size != None:
		h, w = image.shape[:2]
		if h > image_size:
			image = imutils.resize(image, height=image_size)
		elif w > image_size:
			image = imutils.resize(image, width=image_size)
	if rotation_angle != None:
		image = imutils.rotate_bound(image, rotation_angle)
	if padding == True:
		image = padding_image(image)
	return image


def padding_image(image):
	h, w, c = image.shape
	new_size = (h+h//6, w+w//6, c)
	new_image = np.zeros(new_size)
	new_image = new_image + np.random.uniform(size=new_size) * np.mean(image)
	new_image[h//12:h+h//12, w//12:w+w//12, :] = image
	return np.uint8(new_image)


def calculate_angle_vector_and_vertical_vector(vector):
	x, y = vector
	vertical_vector = np.array([0, 1])
	vector = np.array(vector)
	u, v = vertical_vector, vector
	c = dot(u,v)/norm(u)/norm(v) 
	angle = arccos(clip(c, -1, 1))
	if x < 0:
		angle = 2*math.pi - angle 
	if angle == 2*math.pi:
		angle = 0
	return angle


def center_of_4points(points):
	(x1,y1), (x2,y2), (x3,y3), (x4, y4) = points
	xi = ((x1*y2-y1*x2)*(x3-x4) - (x1-x2)*(x3*y4-y3*x4))/((x1-x2)*(y3-y4)-(y1-y2)*(x3-x4))
	yi = ((x1*y2-y1*x2)*(y3-y4) - (y1-y2)*(x3*y4-y3*x4))/((x1-x2)*(y3-y4)-(y1-y2)*(x3-x4))
	return xi, yi


def get_angle_and_vector_length(five_point_corrds):
	(x1,y1), (x2,y2), (x3,y3), (x4, y4), (x_nose, y_nose) = five_point_corrds
	four_points = (x1,y1), (x2,y2), (x3,y3), (x4, y4)
	x_intersec, y_intersec = center_of_4points(four_points)
	vector = (x_nose-x_intersec, y_nose-y_intersec)
	angle = calculate_angle_vector_and_vertical_vector(vector)
	vector_length = np.linalg.norm(vector)
	return angle, vector_length


def get_point_coords(points):
	x1 = points[0]
	y1 = points[0 + 5]
	x4 = points[1]
	y4 = points[1 + 5]
	x_nose = points[2]
	y_nose = points[2 + 5]
	x3 = points[3]
	y3 = points[3 + 5]
	x2 = points[4]
	y2 = points[4 + 5]
	return (x1,y1), (x2,y2), (x3,y3), (x4, y4), (x_nose, y_nose)


def gen_random_str():
	fname = uuid.uuid5(uuid.NAMESPACE_OID, str(datetime.datetime.now()))
	return str(fname)
