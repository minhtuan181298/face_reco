# -*- coding: utf-8 -*-
from __future__ import division, print_function
import sys
import numpy as np
from sklearn import model_selection, svm
from sklearn.linear_model import LogisticRegression
from grd_face_recognition import config


class NoveltyDetectionSingle:

    def __init__(self, known_class, novelty_class):
        self.theta = []
        self.known_class = known_class
        self.novelty_class = novelty_class
        set_known_class = set(known_class)
        self.set_known_class = set_known_class
        known_class_to_sub_class = dict()
        sub_class_to_known_class = dict()
        for i in range(len(known_class)):
            known_class_to_sub_class[known_class[i]] = i
            sub_class_to_known_class[i] = known_class[i]

        self.known_class_to_sub_class = known_class_to_sub_class
        self.sub_class_to_known_class = sub_class_to_known_class
        self.n_class = len(set_known_class)+len(set(novelty_class))
        self.scale = 0.01
        self.norm = 0.01  # min(0.001*self.n_class,0.01)

    def fit(self, X, y):
        X_known = []
        X_novelty = []
        y_known = []
        y_novelty = []
        for i in range(len(y)):
            if y[i] in self.set_known_class:
                X_known.append(X[i])
                y_known.append(y[i])
            else:
                X_novelty.append(X[i])
                y_novelty.append(y[i])

        test_size = len(y_novelty)
        X_svm, X_h, y_svm, y_h = model_selection.train_test_split(X_known, y_known,
                                                                  test_size=test_size, stratify=y_known, random_state=23, shuffle=True)

        y_svm_train = [self.known_class_to_sub_class[c] for c in y_svm]

        svc = LogisticRegression(random_state=23, solver='lbfgs', max_iter=150,
                                 class_weight='balanced', multi_class='multinomial')
        svc.fit(X_svm, y_svm_train)
        self.svc = svc

        X_next = []
        y_next = []
        for x, y in zip(X_h, y_h):
            y_pred_proba = svc.predict_proba([x])[0]
            y_pred_proba_sorted = sorted(y_pred_proba)
            theta_s = self.scale * \
                y_pred_proba_sorted[-1] / (y_pred_proba_sorted[-2]+self.norm)
            theta_v = y_pred_proba_sorted[-1]
            X_next.append([theta_v, theta_s])
            y_next.append(0)
        for x, y in zip(X_novelty, y_novelty):
            y_pred_proba = svc.predict_proba([x])[0]
            y_pred_proba_sorted = sorted(y_pred_proba)
            theta_s = self.scale * \
                y_pred_proba_sorted[-1] / (y_pred_proba_sorted[-2]+self.norm)
            theta_v = y_pred_proba_sorted[-1]
            X_next.append([theta_v, theta_s])
            y_next.append(1)

        # training SVM
        svc_novelty = svm.LinearSVC(class_weight='balanced')
        parameters = {'C': [0.1, 1, 2]}
        novelty_model = model_selection.GridSearchCV(
            svc_novelty, parameters, cv=3, n_jobs=-1)
        novelty_model.fit(X_next, y_next)
        self.novelty_model = novelty_model

    def predict(self, X):
        ret = []
        for seq in X:
            y_pred_proba = self.svc.predict_proba(seq)
            y_pred_proba = np.median(y_pred_proba, axis=0)
            y_pred_proba_sorted = sorted(y_pred_proba)
            theta_s = self.scale * \
                y_pred_proba_sorted[-1] / (y_pred_proba_sorted[-2]+self.norm)
            theta_v = y_pred_proba_sorted[-1]
            ret.append(self.novelty_model.predict([[theta_v, theta_s]])[0])
        return ret


class PredictModel:

    def __init__(self, pca=None):
        self.ensemble = []
        self.pca = pca
        self.threshold = None
        self.n_model = None

    def fit(self, X, y):
        class_ids = range(len(set(y)))
        self.n_model = max(min(len(class_ids), 10), config.MIN_NUM_PEOPLE)
        if self.n_model <= 4:
            self.threshold = 1
        elif self.n_model <= 7:
            self.threshold = 2
        else:
            self.threshold = 3
        X = np.array(X)
        y = np.array(y)
        splitter = model_selection.KFold(
            n_splits=self.n_model, random_state=23, shuffle=True)
        for know_class, novelty_class in splitter.split(class_ids):
            model = NoveltyDetectionSingle(know_class, novelty_class)
            print("Training " + str(len(self.ensemble) + 1) + "th novelty model")
            model.fit(X, y)
            self.ensemble.append(model)

        print("Training classifying model")
        svc = LogisticRegression(random_state=23, solver='lbfgs', max_iter=150,
                                 class_weight='balanced', multi_class='multinomial')
        svc.fit(X, y)
        self.svc = svc

    def predict(self, X):
        ret = []
        for seq in X:
            if self.pca != None:
                seq = self.pca.transform(seq)
            y_pred_proba = self.svc.predict_proba(seq)
            y_pred_proba = np.median(y_pred_proba, axis=0)
            y_pred = np.argmax(y_pred_proba)
            proba = np.max(y_pred_proba)
            count = 0
            for model in self.ensemble:
                if y_pred in model.set_known_class:
                    count += model.predict([seq])[0]
            if count <= self.threshold:  # normal
                ret.append((y_pred, proba))
            else:  # novelty
                ret.append((-1, count / 10.0))
        return ret
