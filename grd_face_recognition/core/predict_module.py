# -*- coding: utf-8 -*-
"""Predict module."""
import os
import sys
import pickle
import traceback


class PredictModule:
    def __init__(self, model_path):
        self.predict_model = None
        self.id2name_map = None
        self.model_path = model_path
        self.load_model()


    def load_model(self):
        try:
            with open(os.path.join(self.model_path, 'predict.pkl'), 'rb') as fi:
                self.predict_model = pickle.load(fi)
            with open(os.path.join(self.model_path, 'id2name_map.pkl'), 'rb') as fi:
                self.id2name_map = pickle.load(fi)
        except:
            raise Exception("NO TRAINED MODEL FOUND")


    def predict(self, seq_vecs, is_reload=False):
        """Predict ID using local models.
        Empty string if is newcomer, otherwise id string
        """
        if is_reload:
            self.load_model(self.model_path)
        y_pred, proba = self.predict_model.predict([seq_vecs])[0]
        if y_pred == -1:  # newcomer
            name = ''
        else:
            name = self.id2name_map[y_pred]
        return name, proba
