# -*- coding: utf-8 -*-
from __future__ import print_function
import os
import cv2
import pickle
import glob
import math
import sys
import time
import shutil
import datetime
import numpy as np
from sklearn import decomposition
from sklearn import preprocessing
from sklearn.utils import shuffle

from grd_face_recognition import config
from grd_face_recognition.core import predict_model
from grd_face_recognition.feature_extraction.resnet import get_embedding


def up_down_sample(list_images_file):
    min_size = config.MIN_NUM_TRAINING_IMAGES
    max_size = config.MAX_NUM_TRAINING_IMAGES

    num_files = len(list_images_file)
    if num_files > max_size:
        list_images_file = shuffle(list_images_file)
        list_images_file = list_images_file[:max_size]
    elif num_files < min_size:
        extra = shuffle(list_images_file)[:min_size-num_files]
        list_images_file.extend(extra)
    return list_images_file


def save_sample_image(image_path, person_name, sample_dir):
    sample_image = cv2.imread(image_path)
    sample_path = os.path.join(sample_dir, person_name)
    if not os.path.exists(sample_path):
        os.makedirs(sample_path)
    cv2.imwrite(os.path.join(sample_path, person_name + '.jpg'), sample_image)


def copy_pending_data(face_label_good, face_pending_dir):
    if not os.path.exists(face_pending_dir):
        os.makedirs(face_pending_dir)

    for user_id in os.listdir(face_pending_dir):
        user_folder = os.path.join(face_pending_dir, user_id)
        user_id = user_id.split('_')[-1]
        dest_path = os.path.join(face_label_good, user_id)
        if not os.path.exists(dest_path):
            os.makedirs(dest_path)
        for filepath in glob.glob(os.path.join(user_folder, '*.jpg')):
            shutil.move(filepath, dest_path)
        shutil.rmtree(user_folder, ignore_errors=True)


def load_template_data(input_dir, vector_dir, sample_dir):
    X = []
    name = []
    all_person_folder = glob.glob(os.path.join(input_dir, '*'))
    batch_size = 64

    for person_folder in all_person_folder:
        person_name = person_folder.split(os.sep)[-1]
        list_images_file = glob.glob(os.path.join(person_folder, '*.jpg')) + \
            glob.glob(os.path.join(person_folder, '*/*.jpg'))
        if len(list_images_file) < 8:
            print("Too few images for training, user: " + person_name)
            continue
        list_images_file = up_down_sample(list_images_file)
        save_sample_image(list_images_file[0], person_name, sample_dir)

        # get saved vector
        tmp_list = []
        for fn in list_images_file:
            basename = os.path.basename(fn)
            pkl_file = os.path.join(vector_dir, person_name, basename + '.pkl')
            if os.path.exists(pkl_file):
                with open(pkl_file, 'rb') as fi:
                    v = pickle.load(fi)
                    X.append(v)
                    name.append(person_name)
            else:
                tmp_list.append(fn)

        list_images_file = tmp_list

        # get new vectors
        datalen = len(list_images_file)
        n_batch = int(math.ceil(1.0 * datalen / batch_size))
        for i in range(n_batch):
            start = i * batch_size
            end = min((i + 1) * batch_size, datalen)
            batch = []
            for filename in list_images_file[start:end]:
                img = cv2.imread(filename)
                batch.append(img)
            response = get_embedding(batch)
            for j in range(len(response)):
                v = response[j]
                X.append(v)
                name.append(person_name)
                fn = list_images_file[start + j]
                basename = os.path.basename(fn)
                pkl_dir = os.path.join(vector_dir, person_name)
                if not os.path.exists(pkl_dir):
                    os.makedirs(pkl_dir)
                pkl_file = os.path.join(pkl_dir, basename + '.pkl')
                with open(pkl_file, 'wb') as fo:
                    pickle.dump(v, fo)

    return X, name


def train(X, name, model_path, id2name_path, people_file, is_pca=True):
    if is_pca:
        pca = decomposition.PCA(n_components=config.N_COMPONENTS_PCA)
        pca.fit(X)
        X = pca.transform(X)

    set_name = set(name)
    people = open(people_file, 'w')
    for sn in set_name:
        people.write(sn + '\n')

    le = preprocessing.LabelEncoder()
    label = le.fit_transform(name)

    classes = le.classes_
    code = le.transform(classes)
    id2name = dict(zip(code, classes))

    if is_pca:
        model = predict_model.PredictModel(pca)
    else:
        model = predict_model.PredictModel()
    model.fit(X, label)

    if not os.path.exists(os.path.dirname(model_path)):
        os.makedirs(os.path.dirname(model_path))
    with open(model_path, 'wb') as fo:
        pickle.dump(model, fo)

    if not os.path.exists(os.path.dirname(id2name_path)):
        os.makedirs(os.path.dirname(id2name_path))
    with open(id2name_path, 'wb') as fo:
        pickle.dump(id2name, fo)


def start_training(constants):

    start_time = time.time()

    print("Copying pending data")
    copy_pending_data(constants.face_cluster_dir, constants.face_pending_dir)

    print("Getting new data")
    X, name = load_template_data(
        constants.face_cluster_dir, constants.vector_dir, constants.face_sample_dir)

    print("Training predict model")
    train(X, name, constants.predict_model_path,
          constants.id2name_map_path, constants.list_people_file)

    print("--- %s seconds ---" % (time.time() - start_time))
