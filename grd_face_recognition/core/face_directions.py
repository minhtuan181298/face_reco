# -*- coding: utf-8 -*-
# from __future__ import print_function
import cv2
import sys
import os
import glob
import math
import shutil
import numpy as np
from grd_face_recognition import config
from grd_face_recognition.core import utils
from grd_face_recognition.face_detection.face_detection_mtcnn import detect_face, detect_face_all_directions

MIN_SIZE = config.FACE_MIN_SIZE
MAX_SIZE = config.FACE_MAX_SIZE

MIN_FACE_DIRECTION_CENTER = 6
MIN_FACE_DIRECTION_UP = 4
MIN_FACE_DIRECTION_LOW = 0
MIN_FACE_DIRECTION_RIGHT = 6
MIN_FACE_DIRECTION_LEFT = 6
MIN_FACE_DIRECTION_DICT = {0: MIN_FACE_DIRECTION_CENTER, 1: MIN_FACE_DIRECTION_LOW,
                           2: MIN_FACE_DIRECTION_RIGHT, 3: MIN_FACE_DIRECTION_UP, 4: MIN_FACE_DIRECTION_LEFT}
MAX_FACE_DIRECTION_DICT = {0: 12, 1: 12, 2: 12, 3: 12, 4: 12}
directions = ["trực diện", "quay xuống", "quay trái", "quay lên", "quay phải"]


def get_direction_img_file_basename(old_basename, region_number, vector_length):
    basename = old_basename + \
        '_' + str(region_number) + '_' + \
        str(vector_length)[:min(5, len(str(vector_length)))
                           ] + '_' + config.mark_processed
    return basename


def evaluate_region_direction(face_location, x1, y1, x2, y2, x3, y3, x4, y4, x_nose, y_nose, frame):
    (l, t, r, b) = face_location
    face_size = (b - t)
    top_limit = 0.07*face_size
    bottom_limit = 0.035*face_size
    five_point_corrds = (x1, y1), (x2, y2), (x3,
                                             y3), (x4, y4), (x_nose, y_nose)
    angle, vector_length = utils.get_angle_and_vector_length(five_point_corrds)
    region_number = min(int(angle/(math.pi/4)), 7)
    updated_region_number = region_number

    if (region_number in [0, 1, 6, 7] and vector_length < bottom_limit):
        new_region = 0
        updated_region_number = -1
    elif (region_number in [2, 5] and vector_length < 0.03*face_size):
        new_region = 0
        updated_region_number = -1
    elif region_number in [7, 0]:
        new_region = 1
    elif region_number in [1, 2]:
        new_region = 2
    elif region_number in [3]:
        if angle/(math.pi/4) > 3.6 and vector_length > top_limit:
            new_region = 3
        elif angle/(math.pi/4) <= 3.4 and vector_length > 0.07*face_size:
            new_region = 2
        else:
            new_region = 0
            updated_region_number = -1
    elif region_number in [4]:
        if angle/(math.pi/4) < 4.4 and vector_length > top_limit:
            new_region = 3
        elif angle/(math.pi/4) >= 4.6 and vector_length > 0.07*face_size:
            new_region = 4
        else:
            new_region = 0
            updated_region_number = -1
    elif region_number in [5, 6]:
        new_region = 4
    else:
        # print(region_number)
        pass
    return new_region, vector_length/face_size, updated_region_number


def check_false_condition(region_number, region_counter):
    if region_counter[region_number] < MIN_FACE_DIRECTION_DICT[region_number]:
        return True
    return False


def backup_img(img, image_file, raw_person_upload_folder):
    try:
        shutil.move(image_file, raw_person_upload_folder)
    except:
        try:
            random_name = np.random.randint(0, 10000)
            cv2.imwrite(os.path.join(raw_person_upload_folder,
                                     str(random_name) + '.jpg'), img)
            os.remove(image_file)
        except:
            pass


def get_require_direction(indir, backup_dir=None, face_detection_threshold=None, face_detection_factor=None):
    user_id = indir.split(os.sep)[-1]
    outdir = indir
    if backup_dir is not None:
        raw_person_upload_folder = os.path.join(backup_dir, user_id)
        if not os.path.exists(raw_person_upload_folder):
            os.makedirs(raw_person_upload_folder)
    # not_detected_face_folder = 'data/not_detected_face_folder'
    # if os.path.exists(not_detected_face_folder):
    # 	os.system('rm -rf ' + not_detected_face_folder)
    # os.makedirs(not_detected_face_folder)

    region_counter = [0]*5
    files_of_region = dict()
    for i in range(5):
        files_of_region[i] = []
    vecto_length_of_region = dict()
    for i in range(5):
        vecto_length_of_region[i] = []
    list_images_file = glob.glob(os.path.join(
        indir, '*/*.jpg')) + glob.glob(os.path.join(indir, '*.jpg'))
    deleted_files = []
    # print('indir: ', indir)
    # print('n_file in uploaded image: ', len(list_images_file))
    n_not_detected_face = 0
    n_not_satify_shape_condition = 0
    rotation_angle = None
    n_img_to_find_rotation_angle = 0
    n_low_quality_ignore = 0

    new_file_count = 0
    for image_file in list_images_file:
        filename = os.path.split(image_file)[-1]
        filename = filename[:filename.rfind('.')]
        postfix = filename.split('_')
        if len(postfix) >= 3 and postfix[-1] == config.mark_processed:
            direction = int(postfix[-3])
            vector_length = float(postfix[-2])
            region_counter[direction] += 1
            files_of_region[direction].append(image_file)
            vecto_length_of_region[direction].append(vector_length)
            continue
        else:
            new_file_count += 1
        origin_img = cv2.imread(image_file)
        if origin_img is None:
            continue
        img = origin_img.copy()

        if backup_dir is not None:
            backup_img(img, image_file, raw_person_upload_folder)

        img = utils.preprocess_image(img, image_size=config.MAX_IMAGE_SIZE)
        if rotation_angle is None:
            face_detection_all_direction_threshold = [0.5, 0.7, 0.9]
            face_detection_all_direction_factor = 0.75
            location, points, angle = detect_face_all_directions(
                img, MIN_SIZE, MAX_SIZE, face_detection_all_direction_threshold, face_detection_all_direction_factor)
            rotation_angle = angle
            if rotation_angle is None:
                n_img_to_find_rotation_angle += 1
                deleted_files.append(image_file)
                continue
            img = utils.preprocess_image(img, rotation_angle=rotation_angle)
            origin_img = utils.preprocess_image(
                origin_img, rotation_angle=rotation_angle)
        else:
            img = utils.preprocess_image(img, rotation_angle=rotation_angle)
            origin_img = utils.preprocess_image(
                origin_img, rotation_angle=rotation_angle)
            location, points = detect_face(
                img, MIN_SIZE, MAX_SIZE, face_detection_threshold, face_detection_factor)
        cv2.imwrite(image_file, origin_img)

        if len(location) > 0:

            max_size = -1
            best_index = -1
            for i in range(len(location)):
                (l, t, r, b) = location[i]
                size = (r-l)*(b-t)
                if size > max_size:
                    max_size = size
                    best_index = i

            face_location = location[best_index]
            face_img = img[t:b, l:r]
            cv2.imwrite(image_file, face_img)

            if face_img.shape[0] > 0 and face_img.shape[1] > 0:
                quality_threshold = [50, 280, 50]
                is_low_quality = utils.low_quality(face_img, quality_threshold)
                if not is_low_quality:
                    (x1, y1), (x2, y2), (x3, y3), (x4, y4), (x_nose,
                                                             y_nose) = utils.get_point_coords(points[:, best_index])

                    new_region, vector_length, _ = evaluate_region_direction(
                        face_location, x1, y1, x2, y2, x3, y3, x4, y4, x_nose, y_nose, img)

                    region_counter[new_region] += 1
                    files_of_region[new_region].append(image_file)
                    vecto_length_of_region[new_region].append(vector_length)
                else:
                    deleted_files.append(image_file)
                    n_low_quality_ignore += 1
            else:
                deleted_files.append(image_file)
                n_not_satify_shape_condition += 1
        else:
            deleted_files.append(image_file)
            n_not_detected_face += 1
            basename = os.path.basename(image_file)
            # cv2.imwrite(os.path.join(not_detected_face_folder, basename), origin_img)
    # print('new file =', new_file_count)
    # print('n_img_to_find_rotation_angle = ', n_img_to_find_rotation_angle)
    # print('n_not_detected_face = ', n_not_detected_face)
    # print('n_not_satify_shape_condition = ', n_not_satify_shape_condition)
    # print('n_low_quality_ignore = ', n_low_quality_ignore)
    n_file_ignore = n_not_detected_face + n_not_satify_shape_condition + \
        n_low_quality_ignore + n_img_to_find_rotation_angle
    # print('n_file_ignore = ', n_file_ignore)

    # print('file consider = ', len(list_images_file) - n_file_ignore)

    final_files_of_region = dict()
    for i in range(5):
        final_files_of_region[i] = []
    final_vecto_length_of_region = dict()
    for i in range(5):
        final_vecto_length_of_region[i] = []

    require_direction = []
    for i in [0, 1, 2, 3, 4]:
        if check_false_condition(i, region_counter):
            require_direction.append(i)
            end_index = region_counter[i]
        else:
            end_index = min(MAX_FACE_DIRECTION_DICT[i], region_counter[i])
        if i == 0:
            reverse = False
        else:
            reverse = True
        file_and_vector_length = [[f, vector_length] for f, vector_length in zip(
            files_of_region[i], vecto_length_of_region[i])]
        file_and_vector_length = sorted(
            file_and_vector_length, key=lambda element: element[1], reverse=reverse)
        if len(file_and_vector_length) > 0:
            for image_file_and_vector_length in file_and_vector_length[:end_index]:
                image_file, vector_length = image_file_and_vector_length
                final_files_of_region[i].append(image_file)
                final_vecto_length_of_region[i].append(vector_length)
            for f, vector_length in file_and_vector_length[end_index:]:
                deleted_files.append(f)

    image_number = 0
    for i in range(5):
        # print('Huong ' + str(i) + ': ', region_counter[i], len(final_files_of_region[i]))
        image_number += len(final_files_of_region[i])
    # print('image_number = ', image_number)

    for i in range(5):
        for image_file, vector_length in zip(final_files_of_region[i], final_vecto_length_of_region[i]):
            basename = os.path.basename(image_file)
            basename = basename[:basename.rfind('.')]
            postfix = basename.split('_')
            if len(postfix) >= 3 and postfix[-1] == config.mark_processed:
                write_file = os.path.join(outdir, basename + '.jpg')
            else:
                new_basename = get_direction_img_file_basename(
                    basename, i, vector_length)
                write_file = os.path.join(outdir, new_basename + '.jpg')
            img = cv2.imread(image_file)
            os.remove(image_file)
            # os.system("rm -f "+image_file)
            cv2.imwrite(write_file, img)

    for image_file in deleted_files:
        os.remove(image_file)
        # os.system("rm -f "+ image_file)
    # print('n_deleted_image: ', len(deleted_files))
    return require_direction


def is_satify_direction(indir, direction, face_detection_threshold=None, face_detection_factor=None):
    require_directions = get_require_direction(
        indir, face_detection_threshold, face_detection_factor)
    if direction in require_directions:
        return False, require_directions
    else:
        return True, require_directions


def is_satify_direction_2(indir, direction, face_detection_threshold=None, face_detection_factor=None):
    require_directions = get_require_direction(
        indir, face_detection_threshold, face_detection_factor)
    list_images_file = glob.glob(os.path.join(
        indir, '*/*.jpg')) + glob.glob(os.path.join(indir, '*.jpg'))
    for image_file in list_images_file:
        filename = os.path.split(image_file)[-1]
        filename = filename[:filename.rfind('.')]
        postfix = filename.split('_')
        if len(postfix) >= 3 and postfix[-1] == config.mark_processed:
            _direction = int(postfix[-3])
            if _direction > direction:
                os.remove(image_file)
        else:
            os.remove(image_file)
    if direction in require_directions:
        return False, require_directions
    else:
        return True, require_directions
