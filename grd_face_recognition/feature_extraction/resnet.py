#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Face embedding Thrift server."""
from __future__ import print_function
import cv2
import os
from keras.models import load_model
from keras import backend as K
import numpy as np
import tensorflow as tf


def preprocess_input(x, data_format=None, version=1):
    if data_format is None:
        data_format = K.image_data_format()
    assert data_format in {'channels_last', 'channels_first'}

    if version == 1:
        if data_format == 'channels_first':
            x = x[:, ::-1, ...]
            x[:, 0, :, :] -= 93.5940
            x[:, 1, :, :] -= 104.7624
            x[:, 2, :, :] -= 129.1863
        else:
            x = x[..., ::-1]
            x[..., 0] -= 93.5940
            x[..., 1] -= 104.7624
            x[..., 2] -= 129.1863

    elif version == 2:
        if data_format == 'channels_first':
            x = x[:, ::-1, ...]
            x[:, 0, :, :] -= 91.4953
            x[:, 1, :, :] -= 103.8827
            x[:, 2, :, :] -= 131.0912
        else:
            x = x[..., ::-1]
            x[..., 0] -= 91.4953
            x[..., 1] -= 103.8827
            x[..., 2] -= 131.0912
    else:
        raise NotImplementedError

    return x


def get_embedding(numpy_imgs):
    images= []
    for img in numpy_imgs:
        img = cv2.resize(img, (image_size, image_size))
        img = img.astype('float64')
        img = img[:, :, ::-1]
        images.append(img)

    images = np.array(images)
    images = preprocess_input(images, version=1)
    with graph.as_default():
        embeddings = vgg_features.predict(images)
    embeddings.shape = (-1, 2048)
    return embeddings.tolist()


global graph
global vgg_features
global image_size

with tf.Graph().as_default() as graph:
    model_path = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'model/reduce_model.h5')
    vgg_features = load_model(model_path)
    image_size = 224
    print('Loaded model')
