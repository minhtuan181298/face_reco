import cv2
import os
import numpy as np
from imutils import rotate_bound


proto = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'models/deploy.prototxt')
model = os.path.join(os.path.abspath(os.path.dirname(__file__)), \
									'models/res10_300x300_ssd_iter_140000.caffemodel')
net = cv2.dnn.readNetFromCaffe(proto, model)
conf = 0.8


def detect_face(image):
	(h, w) = image.shape[:2]
	blob = cv2.dnn.blobFromImage(cv2.resize(image, (300, 300)), 1.0, \
								(300, 300), (104.0, 177.0, 123.0))
	net.setInput(blob)
	detections = net.forward()
	faces=[]
	for i in range(0, detections.shape[2]):
		confidence = detections[0, 0, i, 2]
		if confidence > conf:
			box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
			(startX, startY, endX, endY) = box.astype("int")
			startX = max(0, startX)
			startY = max(0, startY)
			endX = min(w, endX)
			endY = min(h, endY)
			faces.append([startX, startY, endX, endY])			
	return faces


def detect_face_all_directions(img):
	rotation_angle = None
	for angle in [0, 270, 90, 180]:
		image = rotate_bound(img.copy(), angle)
		ret = detect_face(image)
		if len(ret) > 0:
			rotation_angle = angle
			break
	return ret, rotation_angle
