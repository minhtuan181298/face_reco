from __future__ import print_function
import tensorflow as tf
import sys
import os
from grd_face_recognition import config
import grd_face_recognition.face_detection.detect_face as mtcnn
from imutils import rotate_bound


gpu_memory_fraction = 0.36
with tf.Graph().as_default():
	gpu_options = tf.GPUOptions(
		per_process_gpu_memory_fraction=gpu_memory_fraction)
	sess = tf.Session(config=tf.ConfigProto(
		gpu_options=gpu_options, log_device_placement=False))
	with sess.as_default():
		abs_path_model = os.path.join(os.path.abspath(os.path.dirname(__file__)), "models")
		pnet, rnet, onet = mtcnn.create_mtcnn(sess, abs_path_model)

default_threshold = config.MTCNN_THRESHOLD
default_factor = config.MTCNN_FACTOR


def to_rgb(img):
	w, h = img.shape
	ret = np.empty((w, h, 3), dtype=np.uint8)
	ret[:, :, 0] = ret[:, :, 1] = ret[:, :, 2] = img
	return ret


def detect_face(img, min_size=(40, 40), max_size=(800, 800), face_detection_threshold=None, face_detection_factor=None):
	if face_detection_threshold is None:
		face_detection_threshold = default_threshold
	if face_detection_factor is None:
		face_detection_factor = default_factor
	minsize = min(min_size[0], min_size[1])  # minimum size of face
	if img.ndim < 2:
		print('Unable to detect face: img.ndim < 2')
		return None
	if img.ndim == 2:
		img = to_rgb(img)
	img = img[:, :, 0:3]
	bounding_boxes, points = mtcnn.detect_face(img, minsize, pnet, rnet, onet, face_detection_threshold, face_detection_factor)
	ret = []
	for face_position in bounding_boxes:
		face_position = face_position.astype(int)
		ret.append((face_position[0], face_position[1], face_position[2], face_position[3]))
	return ret, points


def detect_face_all_directions(img, min_size=(40, 40), max_size=(800, 800), face_detection_threshold=None, face_detection_factor=None):
	rotation_angle = None
	for angle in [0, 270, 90, 180]:
		image = rotate_bound(img.copy(), angle)
		ret, points = detect_face(image, min_size, max_size, face_detection_threshold, face_detection_factor)
		if len(ret) > 0:
			rotation_angle = angle
			break
	return ret, points, rotation_angle