import numpy as np
from grd_face_recognition.anti_spoofing.predict_patch import predict_spoof_patch
from grd_face_recognition.anti_spoofing.predict_mobilenet import predict_spoof_mobile
from grd_face_recognition.face_detection.face_detection_ssd import detect_face


def predict_spoofing(frame):
	faces = detect_face(frame)
	frame = frame.copy()
	pred = None
	if len(faces) > 0:
		rects = []
		idx = np.argmax(np.array([(b-t)*(r-l) for (l,t,r,b) in faces]))
		startX, startY, endX, endY = faces[idx]
		roi = frame[startY:endY,startX:endX]
		pred1 = predict_spoof_patch(roi)
		pred2 = predict_spoof_mobile(frame)
		pred = 4 * pred1 + pred2
		pred = 1 if pred < 0.75 else 0
	return pred
