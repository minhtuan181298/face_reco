import numpy as np
import cv2
import os
import tensorflow as tf
from keras.models import load_model


with tf.Graph().as_default() as graph:
	with tf.Session().as_default() as sess:
		model_path = os.path.join(os.path.abspath(os.path.dirname(__file__)), \
								  'models/patches32x64_SiW_norm_mean_each-19-0.93.hdf5')
		model = load_model(model_path)


def norm_data(vecs):
    vecs = vecs - vecs.mean(axis=(1,2), keepdims=True)
    return vecs


def predict_spoof_patch(roi):
	vecs = []
	W, H, D = roi.shape	

	img_ycrcb = cv2.cvtColor(roi, cv2.COLOR_BGR2YCR_CB)
	img_hsv = cv2.cvtColor(roi, cv2.COLOR_BGR2HSV)                

	if roi.shape[1]-64<=0 or roi.shape[0]-64<=0:
		return 500

	X = np.random.randint(0,roi.shape[1]-64, size=32)
	Y = np.random.randint(0,roi.shape[0]-64, size=32)

	patches_ycrcb = []
	for i in range(8):
		patches_ycrcb.append(img_ycrcb[Y[i]:Y[i]+64, X[i]:X[i]+64])
	vecs.extend(patches_ycrcb)

	patches_hsv = []
	for i in range(8, 16):
		patches_hsv.append(img_hsv[Y[i]:Y[i]+64, X[i]:X[i]+64])
	vecs.extend(patches_hsv)

	vecs = np.array(vecs)
	vecs = norm_data(vecs)

	with graph.as_default():
		with sess.as_default():
			preds = model.predict(vecs)
	ave_preds = np.mean(preds)
	return ave_preds
