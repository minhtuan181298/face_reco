import cv2
import os
import tensorflow as tf
from keras.models import load_model


with tf.Graph().as_default() as graph:
	with tf.Session().as_default() as sess:
		model_path = os.path.join(os.path.abspath(os.path.dirname(__file__)), \
								  'models/mobilenet_freeze100-25-1.00.hdf5')
		model = load_model(model_path)


def predict_spoof_mobile(image):
	vecs =[]
	image = cv2.resize(image,(224,224))
	image = image.reshape((1, 224, 224, 3))
	image = image * 1. / 255
	with graph.as_default():
		with sess.as_default():
			pred = model.predict([image])[0][1]
	return pred
