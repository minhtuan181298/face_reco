#!/bin/bash
export PYTHONUNBUFFERED=1

echo "Current dir=$PWD"
echo ""

PORT=31003

echo 'Killing GRPC Service Server'
kill `lsof -a -t -i:$PORT`
sleep 3
echo 'Check if server still alive'
lsof -a -i:$PORT

echo "Start GRPC Checkin Service on port $PORT"
nohup python3 grpc_checkin_service.py &> log/grpc_service.log &

echo 'Check if service have been started'
sleep 5
lsof -a -i:$PORT
