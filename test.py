# -*- coding: utf-8 -*-
from __future__ import print_function
import sys
import time
import config
from imageio import imread
import grpc
from grpc_classes import face_recog_pb2, face_recog_pb2_grpc
import os
import glob
from utils import padding_image, encode_np_array


channel = grpc.insecure_channel("localhost:31003")
stub = face_recog_pb2_grpc.FaceRecogServiceStub(channel)


def familiar(path):
    wrong = 0
    filepaths = glob.glob(os.path.join(path, '*/*.jpg'))
    for fp in filepaths:
        name = fp.split(os.sep)[-2]
        image = imread(fp)
        image = padding_image(image)
        request = face_recog_pb2.Images(frames=encode_np_array([image]))
        response = stub.recognize(request)
        names = response.names
        if not ((name in names and len(names) == 1) or len(names) == 0):
            wrong += 1
            print("Wrong recognition: ", wrong)
            print("Predict: ", names)
            print("Label: ", name)
            print(fp)
    print("Number of wrong recognition: ", wrong)
    # print("Error rate: ", wrong / len(filepaths))


def strange(path):
    wrong = 0
    filepaths = glob.glob(os.path.join(path, '*/*.jpg'))
    for fp in filepaths:
        name = fp.split(os.sep)[-2]
        image = imread(fp)
        image = padding_image(image)
        request = face_recog_pb2.Images(frames=encode_np_array([image]))
        response = stub.recognize(request)
        names = response.names
        if not (('newcomer' in names and len(names) == 1) or len(names) == 0):
            wrong += 1
            print("Wrong recognition: ", wrong)
            print("Predict: ", names)
            print("Label: ", name)
            print(fp)
    print("Number of wrong recognition: ", wrong)
    # print("Error rate: ", wrong / len(filepaths))


def diversity_check(path):
    request = face_recog_pb2.DiversityRequest(path=path)
    response = stub.check_diversity(request)
    print(response.is_satisfied)
    print(response.mess)


def delete(name):
    request = face_recog_pb2.DeleteRequest(name=name)
    response = stub.delete(request)
    print(response.is_success, response.mess)


def train():
    request = face_recog_pb2.TrainingRequest()
    response = stub.train(request)
    print(response.is_success, response.mess)


train()
# familiar("/data/uploaded_images")
# strange("/data/uploaded_images")
# diversity_check("/data/uploaded_images")
# delete("nguyennt3")
