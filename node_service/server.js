const express = require("express");
const cors = require("cors");
const grpc = require("grpc");
const UploadService = grpc.load("face_recog.proto").FaceRecogService;
const client = new UploadService(
  "localhost:31003",
  grpc.credentials.createInsecure()
);
const app = express();
const server = require("http").Server(app);
const io = require("socket.io")(server, {
  cors: {
    origin: "http://localhost:3000",
  },
});

const connectMongodb = require("./connectMongodb.js");
const { ErrorCode } = require("./constants");
let EmployeesModel = require("./models/EmployeesModel");

const port = 8888;
connectMongodb();

app.use(cors());
// app.use(express.json());
// app.use(express.urlencoded({ extended: true }));
app.use(express.json({ limit: "50mb" }));
app.use(express.urlencoded({ limit: "50mb" }));

require("./models/PositionModel");

const emit_response = (
  socket,
  is_satisfied,
  message,
  direction = null,
  is_error = false
) => {
  socket.emit("upload_response", {
    is_satisfied,
    message,
    direction,
    is_error,
  });
};

io.on("connection", function (socket) {
  console.log("A connection from client");
  socket.on("upload", function (data) {
    client.upload(
      { image: data.image, name: data.name, direction: data.direction },
      function (err, res) {
        if (err) {
          emit_response(
            socket,
            false,
            "Lỗi kết nối với service",
            (is_error = true)
          );
        } else {
          emit_response(
            socket,
            res.is_satisfied,
            res.mess,
            data.direction,
            res.is_error
          );
        }
      }
    );
  });
});

app.get("/", (req, res) => res.send("It's working!"));

app.post("/check", (request, response) => {
  client.recognize(
    { frames: request.body.image, os: "ios" },
    async function (err, res) {
      console.log(res);
      if (err) {
        return ErrorCode.Err500(response);
      } else {
        if (res.is_recognized == true) {
          let data = await EmployeesModel.find_User(res.names);
          if (data) {
            return ErrorCode.Err200(response, data);
          } else {
            return ErrorCode.Err201(response);
          }
        } else {
          return ErrorCode.Err202(response);
        }
      }
    }
  );
});

app.post("/upload", (request, response) => {
  console.log(request);
  client.upload(
    {
      image: request.body.image,
      name: request.body.name,
      direction: request.body.direction,
    },
    function (err, res) {
      if (err) {
        response.send(err);
      } else {
        if (res.is_satisfied == true && request.direction == 4) {
          response.send({ is_satisfied: res.is_satisfied, mess: res.mess });
        }
      }
    }
  );
});

app.get("/train", (request, response) => {
  client.train({}, function (err, res) {
    if (err) {
      console.log(err);
      response.send({ check: false });
    } else {
      response.send({ mess: res.mess, check: res.is_success });
    }
  });
});

app.delete("/delete", function (request, response) {
  client.delete({ name: data.name }, function (err, res) {
    if (err) {
      response.send("Loi ket noi");
    } else {
      response.send({ is_success: res.is_success, message: res.mess });
    }
  });
});

server.listen(port, function () {
  console.log("Your app running on port " + port);
});
