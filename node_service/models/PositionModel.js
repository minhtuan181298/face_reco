const mongoose = require('mongoose');
const { Schema } = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;

const PositionSchema = new Schema({
    position_name: { type: String, required: true },
    isHidden: { type: Boolean, default: false, required: true },
    created_at: { type: Number, default: Date.now(), required: true },
    updated_at: { type: Number, default: Date.now(), required: true }
});

PositionSchema.statics = {
    createPosition(item) {
        return this.create(item);
    },
    findAllPosition() {
        return this.find({}).exec();
    },
    findPositionById(id) {
        return this.findById(id).exec();
    },
    updated_Position(_id, position) {
        return this.findOneAndUpdate({ _id }, { $set: { position_name: position, updated_at: Date.now() } }, { new: true }).exec()
    },
    hidden_Position(_id) {
        console.log('id', _id)
        return this.findOneAndUpdate({ _id }, { $set: { isHidden: true, updated_at: Date.now() } }).exec()
    },
    show_Position(position_id) {
        return this.findOneAndUpdate({ _id: position_id }, { $set: { isHidden: false } }).exec()
    }
}
module.exports = mongoose.model("Position", PositionSchema);