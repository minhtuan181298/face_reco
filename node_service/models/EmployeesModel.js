const { exec } = require("child_process");
const { Schema } = require("mongoose");
const mongoose = require("mongoose");
const ObjectId = mongoose.Schema.Types.ObjectId;

const EmployeesSchema = new mongoose.Schema({
  full_name: { type: String, required: true },
  gender: { type: Number, required: true },
  dob: { type: Number, required: true },
  idCard: { type: String, required: true },
  address: { type: String, required: true },
  phone_number: { type: String, required: true },
  email: { type: String },
  position_id: {
    type: ObjectId,
    required: true,
    ref: "Position",
  },
  status: { type: Number, required: true, default: 0 },
  domain: { type: String, required: true },
  imageFront: { type: String, required: true },
  imageBack: { type: String, required: true },
  imageSelfie: { type: String, required: true },
  isHidden: { type: Boolean, required: true, default: false },
  created_at: {
    type: Number,
    required: true,
    default: Date.now(),
  },
  updated_at: {
    type: Number,
    required: true,
    default: Date.now(),
  },
});

EmployeesSchema.statics = {
  createEmployee(item) {
    return this.create(item);
  },
  findAllEmployees() {
    return this.find({}).populate("position_id").exec();
  },
  findEmployeeById(id) {
    return this.findById(id).populate("position_id").exec();
  },
  updated_Employee(_id, employee) {
    console.log(_id, employee);
    return this.findOneAndUpdate(
      { _id },
      { $set: employee },
      { new: true }
    ).exec();
  },
  hidden_Employee(employee_id) {
    return this.findOneAndUpdate(
      { _id: employee_id },
      { isHidden: true }
    ).exec();
  },
  show_Employee(employee_id) {
    return this.findOneAndUpdate(
      { _id: employee_id },
      { isHidden: false }
    ).exec();
  },
  find_User(name) {
    return this.findOne({ domain: name }).populate("position_id").exec();
  },
};

module.exports = mongoose.model("Employee", EmployeesSchema);
