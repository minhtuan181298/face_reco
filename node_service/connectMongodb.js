const mongoose = require('mongoose');

const connectMongodb = () => {
    try {
        mongoose.set("useCreateIndex", true);
        mongoose.set('useFindAndModify', false);
        mongoose.connect(
            "mongodb+srv://haidangcnm15:thachxa1@timekeepingmanagement.nckkh.mongodb.net/timekeepingmanagement?retryWrites=true&w=majority",
            {
                useNewUrlParser: true,
                useUnifiedTopology: true,
            }
        )
            .then(() => console.log("connect DB sucessfully"))
    } catch (error) {
        console.log(`Error connect DB: ${error.message}`);
    }
};
module.exports = connectMongodb;