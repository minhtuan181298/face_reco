const ErrCodeRes = (res, code, message, data) => {
    return res.status(code).json({
        data,
        error: {
            code, message
        }
    })
};

const Err500 = (res) => {
    return ErrCodeRes(res, 500, "Internal server error", null)
};

const Err200 = (res, data) => {
    return ErrCodeRes(res, 200, "OK", data)
};

const Err201 = (res) => {
    return ErrCodeRes(res, 201, "Something is not right", null)
};
const Err202 = (res) => {
    return ErrCodeRes(res, 202, "Not exist data", null)
};

module.exports = { ErrCodeRes, Err500, Err200, Err201, Err202 }