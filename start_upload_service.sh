#!/bin/bash

#Database config
export MONGODB_HOST=10.198.54.231
export MONGODB_PORT=27017
export MONGODB_USER=root
export MONGODB_PASS=root123456%40
export MONGODB_DB=facecheckin_dev

cd node_service/

#Service config
export NODE_PORT=31006

echo "Current dir=$PWD"
echo ""

echo 'Killing Upload Service Server'
kill `lsof -a -t -i:$NODE_PORT`
sleep 3
echo 'Check if server still alive'
lsof -a -i:$NODE_PORT

echo 'create log folder'
mkdir -p log

echo "Start Upload Service on port $NODE_PORT"
node socket_server.js &> log/upload_service.log &

echo 'Check if service have been started'
sleep 5
lsof -a -i:$NODE_PORT
